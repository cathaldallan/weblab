# Using NPM tools

1. Download [Nodejs](https://nodejs.org/en/)

2. Install [watch-http-server](https://www.npmjs.com/package/watch-http-server)

    ```bash
    npm install -g watch-http-server
    ```

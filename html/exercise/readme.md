# Exercise instructions

1. Create two html pages:
    * index.html
    * contact-list.html
1. index.html:
    * Create a list of links;
    * One of the links must point to an external website;
    * One of the links must point to `contact-list.html`;
    * Add small images to represent the links;
    * tags that must used: `p`, `ul`, `a`, `img`, `li`
1. contanct-list.html
    * create a table to display a list of phone and email addresses.
    * Format the email with `italic` and `bold`
    * Add a link to go back to `index.html`